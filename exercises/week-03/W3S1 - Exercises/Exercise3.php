<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title>PHP Mathca</title>
</head>
<body>

<div class="container">
    <form id="myForm" action="" method="get">
        <div class="form-group">
            <label for="a">Introduce number for a</label>
            <input type="number" class="form-control" id="a" name="a">
        </div>
        <div class="form-group">
            <label for="b">Introduce number for b</label>
            <input type="number" class="form-control" id="b" name="b">
        </div>
        <div class="form-group">
            <label for="operator">Select an operator:</label>
            <select class="custom-select d-block w-100" name="operator" id="operator">
                <option value="add"> + </option>
                <option value="subtract">  - </option>
                <option value="multiply"> x </option>
                <option value="divide"> / </option>
            </select>
        </div>
        <button type="submit" id="button" class="btn btn-primary">Submit</button>
        <input readonly ="text">
    </form>
</div>

<?php

//if ($_GET["operator"] === 'multiply'){
//    echo $_GET["a"] * $_GET["b"];
//} else if ($_GET["operator"] === "add") {
//    echo $_GET["a"] + $_GET["b"];
//} else if ($_GET["operator"] === "subtract") {
//    echo $_GET["a"] - $_GET["b"];
//} else if ($_GET["operator"] === "divide") {
//    echo $_GET["a"] / $_GET["b"];
//}
?>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
        crossorigin="anonymous"></script>

<!--<script src="calculator.js"></script>-->
</body>
</html>