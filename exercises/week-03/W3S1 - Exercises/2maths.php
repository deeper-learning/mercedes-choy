<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<form action="" method="get">

    <div>
        <label for="a"> Number a </label>
        <input type="number" id="a" name="a">
    </div>
    <div>
        <label for="b"> Number b </label>
        <input type="number" id="b" name="b">

    </div>
    <div>
        <label for="operation"> Type of operation </label>
        <input type="text" id="operation" name="operation">
    </div>
    <br>
    <input type="submit" value="Calculate">
    </div>
</form>

<?php

$userOperationInput = "multiply";

if ($userOperationInput !== $_GET["operation"]) {
    echo "Only multiply is available";
} else{

    echo $_GET["a"] * $_GET["b"];
}

?>

</body>
</html>