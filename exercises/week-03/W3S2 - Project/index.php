<?php error_reporting(0);

class CheckIn
{
    // property declaration
    public $name;
    public $rating;
    public $review;
    public $timestamp;


    public function __construct($name, $rating, $review)
    {
        $this->name = $name;
        $this->rating = $rating;
        $this->review = $review;
        $this->timestamp = date("d-m-Y h:i:sa", $time = time());
//        var_dump($instance);
    }
}

//The htmlspecialchars is for preventing hackers could introduce scripts and change the behaviour of your page.

$review = new CheckIn(htmlspecialchars($_POST["name"]), htmlspecialchars($_POST["rating"]), htmlspecialchars($_POST["review"]));
//var_dump($review);

//if (isset($_POST["name"]) && isset($_POST["rating"]) && isset($_POST["review"])) {
//    $data = $_POST["name"] . '-' . $_POST["rating"] . '-' . $_POST["review"] ."\n";


// Save submission to file
$filename = "Checkin.txt";
if (!file_exists($filename)) {
    $checkInsArray = [];
    array_push($checkInsArray, $review);   //array_push to add the new elements of the array at the end of the array.
    $txtData = serialize($checkInsArray);
    file_put_contents($filename, $txtData);
} else {
    $fileContent = file_get_contents($filename);
    $fileContent = unserialize($fileContent);
    array_push($fileContent, $review);
    $txtData = serialize($fileContent);
    file_put_contents($filename, $txtData);
    $arrayNames = [];
    $arrayRating = [];
    $arrayReview = [];
    $arraytimestamp = [];

    foreach ($fileContent as $checkin) {
        array_push($arrayNames, $checkin->name);
        array_push($arrayRating, $checkin->rating);
        array_push($arrayReview, $checkin->review);
        array_push($arraytimestamp, $checkin->timestamp);
//echo $checkin->name . '<br>';
//echo $checkin->rating . '<br>';
//echo $checkin->review . '<br>';
//echo $checkin->timestamp . '<br>';

// echo "<h3>$checkin->name $checkin->rating</h3>";
// echo "<p>$checkin->review</p>";
// echo "<p><small>$checkin->timestamp</small></p>";


    }
}


// convert string arrays into int arrays
$newArray = array_map( create_function('$value', 'return (int)$value;'),
    $arrayRating);

//var_dump($newArray);



$sum =0; //loop to add all the integers of the rating array to calculate the average.

for ($i = 0; $i < count($newArray); $i++) {
    $sum += $newArray[$i];
}

$rateAverage = floor($sum/count($newArray));
//var_dump($rateAverage);


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="images/mycss.css">
    <title>Document</title>
</head>
<body>
<div class="whole-container">
    <div class="container top-container">

        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="images/dog1.png" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="images/cutedog2.png" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="images/cutedog2.png" class="d-block w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>


    </div>
    <div class="container paragraph">
        <h1>Lorem Ipsum</h1>
        <p class>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At autem distinctio dolore earum eius, error,
            id minus nesciunt, nostrum nulla omnis placeat reprehenderit soluta unde ut. Eveniet harum laboriosam
            provident. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium animi aperiam architecto
            delectus dolor ea eligendi eos eum facere illo ipsa maiores quasi saepe sequi sit tempora vero, vitae
            voluptatibus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis dolorum hic omnis qui
            ratione reiciendis tempore unde. Atque dignissimos expedita, fuga molestiae neque perferendis quaerat
            quisquam reiciendis sint, totam voluptatum?
            <br>
            <br>
            <button id="alertmsg" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"
                    data-whatever="">Check in </button>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New review</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="container">
                        <form id="myForm" action="" method="post">
                            <div class="form-group">
                                <label for="name" class="col-form-label">Name:</label>
                                <input type="text" class="form-control" id="name" name="name" maxlength="10" required>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-form-label">Rating (1-5):</label>
                                <input type="number" class="form-control" id="rating" name="rating" min="1" max="5" required>
                            </div>
                            <div class="form-group">
                                <label for="review" class="col-form-label">Review:</label>
                                <textarea id="review" name="review" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
                                <span id='remainingChar'></span>

                            </div>
                            <div id="hola">
                                <button type="submit" class="btn btn-secondary mb-2" data-dismiss="modal">Close</button>
                                <button type="submit" id="submit" class="btn btn-primary mb-2">Submit review</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="container paragraph">
    <h2>Additional Information</h2>
</div>
<div class="rating">
    <table class="table-rating">
        <tr class="table-row-rating">
            <td class="font">Average Rating</td>
            <td class="din-star-average"></td>
        </tr>
        <tr class="table-row-rating">
            <td class="font">Another Statistic</td>
            <td>78/100</td>
        </tr>
        <tr class="table-row-rating">
            <td class="font">Yet another Statistic</td>
            <td>Something</td>
        </tr>
    </table>
</div>


<div class="container paragraph">
    <h2>Recent Checkins</h2>
</div>

<div class="review">
    <h3>Joe Bloggs</h3>
    <img src="images/stars.png" alt="stars">
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid blanditiis eveniet exercitationem expedita
        fugiat iusto possimus quam reiciendis, saepe, veniam veritatis, vero voluptate voluptates. Delectus ipsam itaque
        sit tempore voluptas.</p>

    <small>03-08-2020 10:40:52am</small>
</div>

<!--loop to display all the review on the web.-->

<?php foreach ($fileContent as $checkin) :?>
<div class="review">

    <?php $str = "U+2B50"; ?>
    <?php $str = preg_replace("/U\+([0-9a-f]{4,5})/mi", '&#x${1}', $str);?>
    <!-- --><?php //echo $str;?>
    <h3><?=$checkin->name;?></h3> <span><?php echo str_repeat($str, $checkin->rating);?></span>
<!--    <h3 class="starR"></h3>-->
<!--    /*str_repeat repeat the string depending on the second value.*/-->
    <p><?=$checkin->review;?></p>
    <p><small><?=$checkin->timestamp;?></small></p>
</div>
<?php endforeach; ?>



<!--<script>-->
<!--    $('#exampleModal').on('show.bs.modal', function (event) {-->
<!--        var button = $(event.relatedTarget) // Button that triggered the modal-->
<!--        var recipient = button.data('whatever') // Extract info from data-* attributes-->
<!--        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).-->
<!--        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.-->
<!--        var modal = $(this)-->
<!--        modal.find('.modal-title').text('New review ' + recipient)-->
<!--        modal.find('.modal-body input').val(recipient)-->
<!--    })-->
<!--</script>-->

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>

    //star rating display
/*
    $(".starR").after("<span class='star-review'></span>");

    let stars;


    if (<?=$checkin->rating?> === 1) {
        stars = " ⭐";
    } else if (<?=$checkin->rating?> === 2) {
        stars = " ⭐ ⭐";
    } else if (<?=$checkin->rating?> === 3) {
        stars = " ⭐ ⭐ ⭐";
    } else if (<?=$checkin->rating?> === 4) {
        stars = " ⭐ ⭐ ⭐ ⭐";
    } else if (<?=$checkin->rating?> === 5) {
        stars = " ⭐ ⭐ ⭐ ⭐ ⭐";
    } else {
        stars = "No rating";
    }
    $(".star-review").text(stars);  //display the result of the if statement in a text on the div.   */


    //average display

    $("<span>").attr("id","star-average").appendTo("td.din-star-average");

    let stars_rev;

    if (<?=$rateAverage?> === 1) {
        stars_rev = " ⭐";
    } else if (<?=$rateAverage?> === 2) {
        stars_rev = " ⭐ ⭐";
    } else if (<?=$rateAverage?> === 3) {
        stars_rev = " ⭐ ⭐ ⭐";
    } else if (<?=$rateAverage?> === 4) {
        stars_rev = " ⭐ ⭐ ⭐ ⭐";
    } else if (<?=$rateAverage?> === 5) {
        stars_rev = " ⭐ ⭐ ⭐ ⭐ ⭐";
    } else {
        stars_rev = "No rating";
    }
    $(".din-star-average").text(stars_rev);  //display the result of the if statement in a text on the div.



  //The JS for the remaining characters on the textarea form.

    $("textarea").keypress(function(){

    if(this.value.length > 159){
    return false;
    }

    $("#remainingChar").text("Remaining characters : " + (159 - this.value.length));
    });


 // success alert message
    $("form").submit(function () {
        // e.preventDefault();
        alert("Review sent");
        $("#alertmsg").after("<div class=\"alert alert-success hh mt-2\" role=\"alert\">\n" +
            "  Review submitted succesfully\n" +
            "</div>")


    });
</script>



<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
        crossorigin="anonymous"></script>
</body>
</html>

