<?php

require_once'../lecture/vendor/autoload.php';

use Carbon\Carbon;

class Form
{

    public $name;
    public $dob;
    public $email;
    public $picture;
    public $submitted;

    public function __construct($name, $dob, $email, $picture)
    {

        $this->name = $name;
        $this->dob = $dob;
        $this->email = $email;
        $this->picture = $picture;
        $this->submitted = Carbon::createFromDate();


    }
}
     if(!empty($_POST)){
         //    var_dump($_POST);
         $data = $_POST['name'] . ' ' . $_POST['dob'] .' '. $_POST['email'] ."\n";
         //    die();  //no se ejecuta nada de lo de abajo.
         file_put_contents(Carbon::now()->timestamp.".txt", $data);

}
      //if(!empty($_FILES)) {
      //    var_dump($_FILES);
      if(!empty($_FILES['picture'])){

      $file = $_FILES['picture'];

//      if ($file['error'] !== UPLOAD_ERR_OK) {
//        //Handle the error
//      }
      //Validate the mime type & size
      //Unique - overwrite existing?
      //filename is safe
      // generate new filename (orignal name in Db)

       $targetPath = 'uploads/' . Carbon::now()->timestamp. '_' .$file['name'];
        if (!move_uploaded_file(
        $file['tmp_name'],
        $targetPath
        )){
        throw new RuntimeException('Failed to move the file');

    }
}

$profile = new Form($_POST['name'],$_POST['dob'],$_POST['email'],$_FILES['picture']);
//var_dump($profile);

//$allimg = array ();
//$dirname = 'uploads/';
//$images = glob($dirname."*.png");
//foreach($images as $image) {
//    $allimg[] = $image;

//looping all the images
$dirname = 'uploads/';
$images = glob($dirname."*.*" );
foreach($images as $image) {
//    echo "<img src='$image'>";

}


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <title>Exercise Week 4</title>
    <style>
      body{

          width: 50%;
          margin: 0 auto 20px;
      }
    </style>

</head>

<body>
<div class="container mt-3">
    <form id="myForm" method="post" enctype="multipart/form-data">
        <div class="form-group col-md-6 mb-3">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Your name" required>
        </div>
        <div class="form-group col-md-6 mb-3">
            <label for="dob">Date of Birth</label>
            <input type="date" class="form-control" id="dob" name="dob" placeholder="Your date of birth" required>
        </div>
        <div class="form-group col-md-6 mb-3">
            <label for="email">Email Address</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Your email" required>
        </div>
        <div class="form-group col-md-6 mb-3">
            <label for="picture">Picture</label>
            <input type="file" class="form-control-file" id="picture" name="picture">
        </div>
        <div class="form-group col-md-6 mb-3">
            <button id="alertmsg" class="btn btn-primary btn-lg" type="submit">Submit</button>
        </div>
    </form>
</div>
<?php
if($file['error'] !== UPLOAD_ERR_OK){
    echo "<div class='alert alert-danger mt-2' role='alert'> Failed to send photo </div>";
}
else {
    echo "<div class='alert alert-success mt-2' role='alert'> Photo successfully submitted </div>";
}
?>



<div class="container">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">

        <div class="carousel-item active">
            <img src="uploads/1613080543_picture.jpg" class="d-block w-100" alt="...">
        </div>
        <?php foreach($images as $image): ?>
        <div class="carousel-item">
            <img src="<?php echo $image?>" class="d-block w-100" alt="">
        </div>
        <?php endforeach;?>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>


</div>
</div>


<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script>
    // $("#myForm").submit(function (e) {
    //     e.preventDefault();
    //     // alert("Review sent");
    //     $("#alertmsg").after("<div class=\"alert alert-success hh mt-2\" role=\"alert\">\n" +
    //         "  Photo submitted succesfully\n" +
    //         "</div>")


    })
</script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

</body>
</html>
