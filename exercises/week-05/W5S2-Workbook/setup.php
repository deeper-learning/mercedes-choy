<?php
require_once'vendor/autoload.php';

$whoops = new\Whoops\Run();
$whoops->pushHandler(
    new \Whoops\Handler\PrettyPageHandler()

);

$whoops->register();

$dotenv =\Dotenv\Dotenv::createImmutable(__DIR__); //DIR means same directory I am.
$dotenv->load();

$logger = new\Monolog\Logger('application');
$logger->pushHandler(
    new \Monolog\Handler\StreamHandler(
        'application.log',
        \Monolog\Logger::WARNING

    )

);



require_once 'classes/Product.php';

try{

    $dbh = new PDO (
        'mysql:dbname=project;host=mysql',
        $_ENV['DBUSERNAME'],
        $_ENV['DBPASSWD']
    );

    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch (PDOException $e){
    //We could log this!

    die('unable to establish a database connection');  //docker the DEEPer DB might not connected

}

