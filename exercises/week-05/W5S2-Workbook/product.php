<?php
require_once 'setup.php';

$productId = $_GET['id'];

$stmt = $dbh->prepare(
    'SELECT id, title, description, image_path FROM product WHERE id=:id'
);
$stmt->execute([
    'id' => $productId
]);
$product = $stmt->fetchObject(Product::class);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <title>Product Detail</title>
</head>
<body>
<div class="container">
    <h1>Product List</h1>
    <table class="table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Description</th>
            <th>Image</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td><?= $product ->id ?></td>
                <td><?= $product ->title ?></td>
                <td><?= $product ->description ?></td>
                <td><img src="<?= $product ->image_path ?>" alt=""></td>
            </tr>
        </tbody>
    </table>
</div>
</body>
</html>