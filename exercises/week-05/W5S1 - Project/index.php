<?php

$servername = 'mysql';
$username = 'root';
$password = 'root';
$dbname = "project";
try {
    $name = $_POST['name'];
    $rating = $_POST['rating'];
    $review = $_POST['review'];
    $submitdate = date("Y-m-d H:i:s");
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

    /* set the PDO error mode to exception */

    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "INSERT INTO checkins (user_name,rating,review,submitted)
    VALUES ('$name', '$rating','$review','$submitdate')";
    $conn->exec($sql);
    $message = "New checkin created successfully";

    $stmt = $conn->prepare(

        'SELECT user_name, rating, review, submitted FROM checkins WHERE id>2'
    );

    $stmt ->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

//    var_dump($data);

    $arrayNames = [];
    $arrayRating = [];
    $arrayReview = [];
    $arrayTimes = [];

    foreach ($data as $datas) {
        array_push($arrayNames, $datas['user_name']);
        array_push($arrayRating, $datas['rating']);
        array_push($arrayReview, $datas['review']);
        array_push($arrayTimes, $datas['submitted']);

    }
//    var_dump($arrayNames)  . '<br>';
//    var_dump($arrayRating)  . '<br>';
//    var_dump($arrayReview)  . '<br>';
//    var_dump($arrayTimes)  . '<br>';

    $avg = $conn->prepare(

        'SELECT avg(rating) FROM checkins;'
    );

    $avg ->execute();
    $average = $avg->fetch(PDO::FETCH_ASSOC);
    $getAverageNumber = $average['avg(rating)'];  // obtain the value of the array $average using the key 'avg(rating)'
    $convertInt = intval($getAverageNumber);  // with this property inval() we can convert the string into a integer.
//    var_dump($getAverageNumber);
//    var_dump($convertInt);


} catch (PDOException $e) {

    echo $sql . "<br>" . $e->getMessage();
}

$conn = null;


//require_once 'database.php';
//
//$reviewAddedSuccess = false;
//
//if(!empty($_POST)) {
//    $name = $_POST['name'];
//    $rating = $_POST['rating'];
//    $review = $_POST['review'];
//    if (!empty($name) && !empty($rating) && !empty($review)) {
//        $stmt = $dbh->prepare(
//
//            "INSERT INTO project (user_name,rating,review) VALUES ($name,$rating,$review)"
//        );
//
//        $stmt->execute([
//            'name' => $name,
//            'rating' => $rating,
//            'review' => $review
//        ]);
//
//        echo '#Rows affected: ' . $stmt->rowCount();
//
//        if ($stmt->rowCount() > 0) {
//            $reviewAddedSuccess = true;
//        }
//    }
//}

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="images/mycss.css">
    <title>Document</title>
</head>
<body>


<div class="whole-container">
    <div class="container top-container">

        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="images/dog1.png" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="images/cutedog2.png" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="images/cutedog2.png" class="d-block w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>



    </div>
    <div class="container paragraph">
        <h1>Lorem Ipsum</h1>
        <p class>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At autem distinctio dolore earum eius, error,
            id minus nesciunt, nostrum nulla omnis placeat reprehenderit soluta unde ut. Eveniet harum laboriosam
            provident. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium animi aperiam architecto
            delectus dolor ea eligendi eos eum facere illo ipsa maiores quasi saepe sequi sit tempora vero, vitae
            voluptatibus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis dolorum hic omnis qui
            ratione reiciendis tempore unde. Atque dignissimos expedita, fuga molestiae neque perferendis quaerat
            quisquam reiciendis sint, totam voluptatum?
            <br>
            <br>
            <button id="alertmsg" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"
                    data-whatever="">Check in </button>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New review</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="container">
                        <form id="myForm" action="" method="post">
                            <div class="form-group">
                                <label for="name" class="col-form-label">Name:</label>
                                <input type="text" class="form-control" id="name" name="name" maxlength="15" required>
                            </div>
                            <div class="form-group">
                                <label for="rating" class="col-form-label">Rating (1-5):</label>
                                <input type="number" class="form-control" id="rating" name="rating" min="1" max="5" required>
                            </div>
                            <div class="form-group">
                                <label for="review" class="col-form-label">Review:</label>
                                <textarea id="review" name="review" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
                                <span id='remainingChar'></span>

                            </div>
                            <div id="hola">
                                <button type="submit" class="btn btn-secondary mb-2" data-dismiss="modal">Close</button>
                                <button type="submit" id="submit" class="btn btn-primary mb-2">Submit review</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--<div class="alert alert-success" role="alert"></div>-->
<div class="container paragraph">
    <h2>Additional Information</h2>
</div>
<div class="rating">
    <table class="table-rating">
        <tr class="table-row-rating">
            <td class="font">Average Rating</td>
            <td class="din-star-average"></td>
        </tr>
        <tr class="table-row-rating">
            <td class="font">Another Statistic</td>
            <td>78/100</td>
        </tr>
        <tr class="table-row-rating">
            <td class="font">Yet another Statistic</td>
            <td>Something</td>
        </tr>
    </table>
</div>


<div class="container paragraph">
    <h2>Recent Checkins</h2>
</div>

<div class="review">
    <h3>Joe Bloggs</h3>
    <img src="images/stars.png" alt="stars">
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid blanditiis eveniet exercitationem expedita
        fugiat iusto possimus quam reiciendis, saepe, veniam veritatis, vero voluptate voluptates. Delectus ipsam itaque
        sit tempore voluptas.</p>

    <small>03-08-2020 10:40:52am</small>
</div>

<!--loop to display all the review on the web.-->

<?php foreach ($data as $datas) :?>
    <div class="review">

        <?php $str = "U+2B50"; ?>
        <?php $str = preg_replace("/U\+([0-9a-f]{4,5})/mi", '&#x${1}', $str);?>
        <!-- --><?php //echo $str;?>
        <h3><?=$datas['user_name'];?></h3> <span><?php echo str_repeat($str, $datas['rating']);?></span>
        <!--    <h3 class="starR"></h3>-->
        <!--    /*str_repeat repeat the string depending on the second value.*/-->
        <p><?=$datas['review'];?></p>
        <p><small><?=$datas['submitted'];?></small></p>
    </div>
<?php endforeach; ?>


<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>

    //star rating display
/*
    $(".starR").after("<span class='star-review'></span>");

    let stars;


    if (<?=$checkin->rating?> === 1) {
        stars = " ⭐";
    } else if (<?=$checkin->rating?> === 2) {
        stars = " ⭐ ⭐";
    } else if (<?=$checkin->rating?> === 3) {
        stars = " ⭐ ⭐ ⭐";
    } else if (<?=$checkin->rating?> === 4) {
        stars = " ⭐ ⭐ ⭐ ⭐";
    } else if (<?=$checkin->rating?> === 5) {
        stars = " ⭐ ⭐ ⭐ ⭐ ⭐";
    } else {
        stars = "No rating";
    }
    $(".star-review").text(stars);  //display the result of the if statement in a text on the div.   */


    //average display

    $("<span>").attr("id","star-average").appendTo("td.din-star-average");

    let stars_rev;

    if (<?=$convertInt?> === 1) {
        stars_rev = " ⭐";
    } else if (<?=$convertInt?> === 2) {
        stars_rev = " ⭐ ⭐";
    } else if (<?=$convertInt?> === 3) {
        stars_rev = " ⭐ ⭐ ⭐";
    } else if (<?=$convertInt?> === 4) {
        stars_rev = " ⭐ ⭐ ⭐ ⭐";
    } else if (<?=$convertInt?> === 5) {
        stars_rev = " ⭐ ⭐ ⭐ ⭐ ⭐";
    } else {
        stars_rev = "No rating";
    }
    $(".din-star-average").text(stars_rev);  //display the result of the if statement in a text on the div.



  //The JS for the remaining characters on the textarea form.

    $("textarea").keypress(function(){

    if(this.value.length > 159){
    return false;
    }

    $("#remainingChar").text("Remaining characters : " + (159 - this.value.length));
    });


 // success alert message
    $("myForm").submit(function () {
        // e.preventDefault();
        alert("Review sent");
        $("#alertmsg").after("<div class=\"alert alert-success hh mt-2\" role=\"alert\">\n" +
            "  Review submitted succesfully\n" +
            "</div>")


    });
</script>



<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
        crossorigin="anonymous"></script>
</body>
</html>

